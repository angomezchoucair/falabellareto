#language:es

  Característica: El usuario desea coomprar un articulo ofertado

    Escenario: el usuario llega al paso de pago de un articulo
      Dado que el usuario se encuentra en la pagina web y selecciona un articulo
      Cuando oprime el boton de agregar a la bolsa
      Y seleccion la opcion de ver bolsa en el pop up
      Y oprime el boton de ir a comprar
      Y llena la inforacion de departamento,ciudad y barrio
      Y ingresa la direccion y oprime el boton ingresar direccion
      Y oprime el boton continuar
      Entonces se muestra las opciones de pago