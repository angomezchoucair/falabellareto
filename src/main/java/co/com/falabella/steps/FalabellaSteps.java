package co.com.falabella.steps;

import co.com.falabella.pageObject.FalabellaPageObject;
import co.com.falabella.utils.CompraLista;
import co.com.falabella.utils.FalabellaExcel;
import co.com.falabella.utils.Scroll;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.support.ui.Select;


import java.util.List;

public class FalabellaSteps {
    FalabellaPageObject falabellaPageObject = new FalabellaPageObject();
    FalabellaExcel falabellaExcel = new FalabellaExcel();
    List<CompraLista> compraListas = falabellaExcel.IngresCredencialesCompra();
    Scroll scroll = new Scroll();

    @Step
    public void abrirNavegador(){
    falabellaPageObject.openUrl(compraListas.get(0).getUrl());
    }
    @Step
    public void seleccionarItem(){
        scroll.scrollAlElemento(falabellaPageObject.getDriver(),falabellaPageObject.getTxtOfertaEspecial());
        falabellaPageObject.getDriver().findElement(falabellaPageObject.getImgNintendo()).click();

    }
    @Step
    public void seleccionarPc(){
        falabellaPageObject.getDriver().findElement(falabellaPageObject.getPcLenovo()).click();
    }
    @Step
    public void agregarAlaBolsa(){
        falabellaPageObject.getDriver().findElement(falabellaPageObject.getBtnAgregarAlaBolsa()).click();
    }
    @Step
    public void verBolsaPopUp(){
        falabellaPageObject.getDriver().findElement(falabellaPageObject.getBtnVerBolsa()).click();
    }
    @Step
    public void irAComprar(){
        falabellaPageObject.getDriver().findElement(falabellaPageObject.getBtnIrAComprar()).click();
    }
    @Step
    public void llenarUbicacion(){
        scroll.scrollAlElemento(falabellaPageObject.getDriver(),falabellaPageObject.getTxtEligeTusOpciones());

        Select dropDowndepa = new Select(falabellaPageObject.getDriver().findElement(falabellaPageObject.getDropRegion()));
        dropDowndepa.selectByVisibleText(compraListas.get(0).getDepartamento());

        Select dropDownCiudad = new Select(falabellaPageObject.getDriver().findElement(falabellaPageObject.getDroptCiudad()));
        dropDownCiudad.selectByVisibleText(compraListas.get(0).getCiudad());

        Select dropDownBarrio = new Select(falabellaPageObject.getDriver().findElement(falabellaPageObject.getDroptBarrio()));
        dropDownBarrio.selectByVisibleText(compraListas.get(0).getBarrio());
    }
    @Step
    public void clickContinuar(){
        falabellaPageObject.getDriver().findElement(falabellaPageObject.getBtnContinuar()).click();
    }
    @Step
    public void ingresaDireccion(){
        falabellaPageObject.getDriver().findElement(falabellaPageObject.getInputDireccion()).sendKeys(compraListas.get(0).getDireccion());
        falabellaPageObject.getDriver().findElement(falabellaPageObject.getInputTipoDeVivienda()).sendKeys(compraListas.get(0).getVivienda());

    }
    @Step
    public void clickIngresarDirec(){
        falabellaPageObject.getDriver().findElement(falabellaPageObject.getBtnIngresarDireccion()).click();
    }
    @Step
    public void clickContinuar2(){
        scroll.scrollAlElemento(falabellaPageObject.getDriver(),falabellaPageObject.getBtnContinuar2());
        falabellaPageObject.getDriver().findElement(falabellaPageObject.getBtnContinuar2()).click();
    }

    @Step
    public String metodoDePago(){
        String pago = falabellaPageObject.getDriver().findElement(falabellaPageObject.getTxtMetodoDePago()).getText();
        return pago;
    }

}
