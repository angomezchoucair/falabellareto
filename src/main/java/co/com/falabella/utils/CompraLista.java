package co.com.falabella.utils;

public class CompraLista {
    private String url;
    private String departamento;
    private String ciudad;
    private String barrio;
    private String direccion;
    private String vivienda;

    public CompraLista(String url, String departamento, String ciudad, String barrio, String direccion, String vivienda) {
        this.url = url;
        this.departamento = departamento;
        this.ciudad = ciudad;
        this.barrio = barrio;
        this.direccion = direccion;
        this.vivienda = vivienda;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getBarrio() {
        return barrio;
    }

    public void setBarrio(String barrio) {
        this.barrio = barrio;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getVivienda() {
        return vivienda;
    }

    public void setVivienda(String vivienda) {
        this.vivienda = vivienda;
    }

    @Override
    public String toString() {
        return "CompraLista{" +
                "url='" + url + '\'' +
                "departamento='" + departamento +
                ", ciudad=" + ciudad +
                ", barrio=" + barrio +
                ", direccion=" + direccion +
                ", vivienda=" + vivienda +
                '}';
    }
}
