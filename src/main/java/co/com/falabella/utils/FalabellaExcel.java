package co.com.falabella.utils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;


public class FalabellaExcel {

    private FileInputStream file;
    private XSSFWorkbook book;
    private XSSFSheet hoja;


    public List<CompraLista> IngresCredencialesCompra(){
        List<CompraLista> rows = new ArrayList<CompraLista>();
        try {
            this.file = new FileInputStream(new File("FalabellaReto.xlsx"));
            this.book = new XSSFWorkbook(file);
            this.hoja = book.getSheet("Hoja1");
            Row row;

            int cant_sols = (int)(hoja.getRow(1).getCell(6).getNumericCellValue());
            for(int i =1; i <cant_sols; i++){
                row = hoja.getRow(i);
                CompraLista nueva = new CompraLista(row.getCell(0).getStringCellValue(),row.getCell(1).getStringCellValue(),
                        row.getCell(2).getStringCellValue(),row.getCell(3).getStringCellValue(),row.getCell(4).getStringCellValue(),row.getCell(5).getStringCellValue());
                rows.add(nueva);
            }

        }catch (Exception e){
            e.printStackTrace();
        }

        return rows;

    }


}
