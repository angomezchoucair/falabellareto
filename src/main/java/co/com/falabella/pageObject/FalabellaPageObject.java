package co.com.falabella.pageObject;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;

public class FalabellaPageObject extends PageObject {
    By noPopUp = By.xpath("/html/body/div[3]//div/div/div[3]/button[1]");
    By imgNintendo = By.xpath("//*[@id=\"grid-card-0\"]/a/div[1]/picture/img");
    By txtOfertaEspecial = By.xpath("//div[contains(p,'Ofertas Especiales')]");
    By pcLenovo = By.xpath("//*[@id=\"testId-pod-displaySubTitle-24724416\"]");
    By btnAgregarAlaBolsa = By.xpath("//button[contains(text(),'Agregar a la Bolsa')]");
    By btnVerBolsa = By.xpath("//div[contains(a,'Ver Bolsa de Compras')]");
    By btnIrAComprar = By.xpath("//button[contains(text(),'Ir a comprar')]");
    By txtEligeTusOpciones = By.xpath("//div[contains(h2,'Elige tus opciones de despacho')]");
    By dropRegion = By.xpath("//select[contains(option,'Por favor selecciona un departamento')]");
    By droptCiudad = By.xpath("//select[@id='ciudad']");
    By droptBarrio = By.xpath("//select[@id='comuna']");
    By btnContinuar = By.xpath("//body/div[3]/div[1]/div[2]/div[1]/section[1]/section[1]/form[1]/div[1]/div[4]/div[1]/button[1]");
    By inputDireccion = By.xpath("//input[@id='address']");
    By inputTipoDeVivienda = By.xpath("//input[@id='departmentNumber']");
    By btnIngresarDireccion =By.xpath("//button[contains(text(),'Ingresar dirección')]");
    By btnContinuar2 = By.xpath("//button[contains(text(),'Continuar')]");
    By txtMetodoDePago = By.xpath("//h2[contains(text(),'Elige tu medio de pago')]");

    public By getNoPopUp() {
        return noPopUp;
    }

    public void setNoPopUp(By noPopUp) {
        this.noPopUp = noPopUp;
    }

    public By getImgNintendo() {
        return imgNintendo;
    }

    public void setImgNintendo(By imgNintendo) {
        this.imgNintendo = imgNintendo;
    }

    public By getTxtOfertaEspecial() {
        return txtOfertaEspecial;
    }

    public void setTxtOfertaEspecial(By txtOfertaEspecial) {
        this.txtOfertaEspecial = txtOfertaEspecial;
    }

    public By getPcLenovo() {
        return pcLenovo;
    }

    public void setPcLenovo(By pcLenovo) {
        this.pcLenovo = pcLenovo;
    }

    public By getBtnAgregarAlaBolsa() {
        return btnAgregarAlaBolsa;
    }

    public void setBtnAgregarAlaBolsa(By btnAgregarAlaBolsa) {
        this.btnAgregarAlaBolsa = btnAgregarAlaBolsa;
    }

    public By getBtnVerBolsa() {
        return btnVerBolsa;
    }

    public void setBtnVerBolsa(By btnVerBolsa) {
        this.btnVerBolsa = btnVerBolsa;
    }

    public By getBtnIrAComprar() {
        return btnIrAComprar;
    }

    public void setBtnIrAComprar(By btnIrAComprar) {
        this.btnIrAComprar = btnIrAComprar;
    }

    public By getTxtEligeTusOpciones() {
        return txtEligeTusOpciones;
    }

    public void setTxtEligeTusOpciones(By txtEligeTusOpciones) {
        this.txtEligeTusOpciones = txtEligeTusOpciones;
    }

    public By getDropRegion() {
        return dropRegion;
    }

    public void setDropRegion(By dropRegion) {
        this.dropRegion = dropRegion;
    }

    public By getDroptCiudad() {
        return droptCiudad;
    }

    public void setDroptCiudad(By droptCiudad) {
        this.droptCiudad = droptCiudad;
    }

    public By getDroptBarrio() {
        return droptBarrio;
    }

    public void setDroptBarrio(By droptBarrio) {
        this.droptBarrio = droptBarrio;
    }

    public By getBtnContinuar() {
        return btnContinuar;
    }

    public void setBtnContinuar(By btnContinuar) {
        this.btnContinuar = btnContinuar;
    }

    public By getInputDireccion() {
        return inputDireccion;
    }

    public void setInputDireccion(By inputDireccion) {
        this.inputDireccion = inputDireccion;
    }

    public By getInputTipoDeVivienda() {
        return inputTipoDeVivienda;
    }

    public void setInputTipoDeVivienda(By inputTipoDeVivienda) {
        this.inputTipoDeVivienda = inputTipoDeVivienda;
    }

    public By getBtnIngresarDireccion() {
        return btnIngresarDireccion;
    }

    public void setBtnIngresarDireccion(By btnIngresarDireccion) {
        this.btnIngresarDireccion = btnIngresarDireccion;
    }

    public By getBtnContinuar2() {
        return btnContinuar2;
    }

    public void setBtnContinuar2(By btnContinuar2) {
        this.btnContinuar2 = btnContinuar2;
    }

    public By getTxtMetodoDePago() {
        return txtMetodoDePago;
    }

    public void setTxtMetodoDePago(By txtMetodoDePago) {
        this.txtMetodoDePago = txtMetodoDePago;
    }
}
