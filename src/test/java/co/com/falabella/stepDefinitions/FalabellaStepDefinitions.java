package co.com.falabella.stepDefinitions;

import co.com.falabella.steps.FalabellaSteps;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;

public class FalabellaStepDefinitions {
    @Steps
    FalabellaSteps falabellaSteps;

    @Dado("^que el usuario se encuentra en la pagina web y selecciona un articulo$")
    public void queElUsuarioSeEncuentraEnLaPaginaWebYSeleccionaUnArticulo() {
    falabellaSteps.abrirNavegador();
    //falabellaSteps.cerrarPopUp();
    falabellaSteps.seleccionarItem();
    falabellaSteps.seleccionarPc();
    }

    @Cuando("^oprime el boton de agregar a la bolsa$")
    public void oprimeElBotonDeAgregarALaBolsa() {
    falabellaSteps.agregarAlaBolsa();
    }

    @Cuando("^seleccion la opcion de ver bolsa en el pop up$")
    public void seleccionLaOpcionDeVerBolsaEnElPopUp() {
    falabellaSteps.verBolsaPopUp();
    }

    @Cuando("^oprime el boton de ir a comprar$")
    public void oprimeElBotonDeIrAComprar() {
    falabellaSteps.irAComprar();
    }

    @Cuando("^llena la inforacion de departamento,ciudad y barrio$")
    public void llenaLaInforacionDeDepartamentoCiudadYBarrio() {
    falabellaSteps.llenarUbicacion();
    falabellaSteps.clickContinuar();


    }

    @Cuando("^ingresa la direccion y oprime el boton ingresar direccion$")
    public void ingresaLaDireccionYOprimeElBotonIngresarDireccion() {
    falabellaSteps.ingresaDireccion();
        falabellaSteps.clickIngresarDirec();
    }

    @Cuando("^oprime el boton continuar$")
    public void oprimeElBotonContinuar(){
        falabellaSteps.clickContinuar2();
    }

    @Entonces("^se muestra las opciones de pago$")
    public void seMuestraLasOpcionesDePago() {
        Assert.assertEquals("Elige tu medio de pago",falabellaSteps.metodoDePago());

    }
}
